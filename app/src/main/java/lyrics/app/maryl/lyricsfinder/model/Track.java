package lyrics.app.maryl.lyricsfinder.model;

/**
 * Created by MARIALENA on 06/06/2018.
 */

public class Track {

    private String track_name, album_name, artist_name, album_coverart, release_date;
    private int track_id;

    public Track(String track_name, String album_name, String artist_name, String album_coverart, String release_date, int track_id) {
        this.track_name = track_name;
        this.album_name = album_name;
        this.artist_name = artist_name;
        this.album_coverart = album_coverart;
        this.release_date = release_date;
        this.track_id = track_id;
    }

    public String getTrack_name() {
        return track_name;
    }

    public void setTrack_name(String track_name) {
        this.track_name = track_name;
    }

    public String getAlbum_name() {
        return album_name;
    }

    public void setAlbum_name(String album_name) {
        this.album_name = album_name;
    }

    public String getArtist_name() {
        return artist_name;
    }

    public void setArtist_name(String artist_name) {
        this.artist_name = artist_name;
    }

    public String getAlbum_coverart() {
        return album_coverart;
    }

    public void setAlbum_coverart(String album_coverart) {
        this.album_coverart = album_coverart;
    }

    public String getRelease_date() {
        return release_date;
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }

    public int getTrack_id() {
        return track_id;
    }

    public void setTrack_id(int track_id) {
        this.track_id = track_id;
    }

    @Override
    public String toString() {
        return "track: " +
                " name: " + track_name +
                ", album_name: " + album_name +
                ", artist_name: " + artist_name +
                ", album_coverart: " + album_coverart +
                ", release_date: " + release_date +
                ", track_id: " + track_id;
    }
}
