package lyrics.app.maryl.lyricsfinder;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import lyrics.app.maryl.lyricsfinder.model.User;

public class UserProfileActivity extends AppCompatActivity implements View.OnClickListener {

    private DatabaseReference usersdb;
    private String userId;
    private EditText textName, textSurname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        textName = findViewById(R.id.editTextName);
        textSurname = findViewById(R.id.editTextSurname);

        usersdb = FirebaseDatabase.getInstance().getReference("Users");

        usersdb.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
                User user = dataSnapshot.child(userId).getValue(User.class);

                TextView txtextName = findViewById(R.id.editTextName);
                TextView txtextSurname = findViewById(R.id.editTextSurname);
                txtextName.setText(user.getName());
                txtextSurname.setText(user.getSurname());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        findViewById(R.id.buttonUpdateProfile).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        DatabaseReference dbref = usersdb.child(userId);

        String textNameR = textName.getText().toString();
        String textSurnameR = textSurname.getText().toString();

        User userUpd = new User(textNameR, textSurnameR);
        dbref.setValue(userUpd);

        Toast.makeText(getApplicationContext(), "Your profile is updated successfully!", Toast.LENGTH_SHORT).show();
    }
}
