package lyrics.app.maryl.lyricsfinder.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import lyrics.app.maryl.lyricsfinder.R;
import lyrics.app.maryl.lyricsfinder.model.Track;

/**
 * Created by MARIALENA on 06/06/2018.
 */

public class TrackAdapter extends ArrayAdapter {

    List list = new ArrayList();

    public TrackAdapter(@NonNull Context context, int resource) {
        super(context, resource);
    }

    public void add(Track object){
        super.add(object);
        list.add(object);
    }

    @Override
    public int getCount(){
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View row;
        row = convertView;
        TrackHolder trackHolder;
        if(row == null){
            LayoutInflater layoutInflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = layoutInflater.inflate(R.layout.row_layout_results, parent, false);
            trackHolder = new TrackHolder();
            trackHolder.track_name = row.findViewById(R.id.tx_track_name);
            trackHolder.artist_name = row.findViewById(R.id.tx_artist);

            row.setTag(trackHolder);
        }
        else{
            trackHolder = (TrackHolder) row.getTag();
        }

        Track track = (Track)this.getItem(position);
        trackHolder.track_name.setText(track.getTrack_name());
        trackHolder.artist_name.setText(track.getArtist_name());

        return row;
    }

    static class TrackHolder{
        TextView track_name, artist_name, track_id;
    }
}
