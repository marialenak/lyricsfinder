package lyrics.app.maryl.lyricsfinder;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
/**
 * Created by MARIALENA on 04/06/2018.
 */

public class SearchActivity extends AppCompatActivity implements View.OnClickListener, NavigationView.OnNavigationItemSelectedListener {

    private EditText searchText;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle btnToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        searchText = findViewById(R.id.search_txt);
        drawerLayout = findViewById(R.id.drawerLayout);
        btnToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close);

        drawerLayout.addDrawerListener(btnToggle);
        btnToggle.syncState();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NavigationView navigationView = findViewById(R.id.navigation_menu);
        navigationView.setNavigationItemSelectedListener(this);

        findViewById(R.id.search_btn).setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(btnToggle.onOptionsItemSelected(item)){
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        if(searchText.length() > 3) {
            Intent searchResultIntent = new Intent(this, SearchResultsActivity.class);
            searchResultIntent.putExtra("searchParameter", searchText.getText().toString().trim());
            startActivity(searchResultIntent);
        }
        else{
            searchText.setError("Three characters are at least required");
            searchText.requestFocus();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {


        switch (item.getItemId()) {

            case R.id.nav_profile: {

                startActivity(new Intent(this, UserProfileActivity.class));
                break;
            }

            case  R.id.nav_favorites: {

                startActivity(new Intent(this, FavoritesActivity.class));
                break;
            }
        }

        return true;
    }
}
