package lyrics.app.maryl.lyricsfinder;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.json.JSONException;
import org.json.JSONObject;

import io.realm.Realm;
import io.realm.RealmResults;
import lyrics.app.maryl.lyricsfinder.adapters.TrackAdapter;
import lyrics.app.maryl.lyricsfinder.model.Favorites;
import lyrics.app.maryl.lyricsfinder.model.Track;

public class FavoritesActivity extends AppCompatActivity {

    private Realm realm;
    private ListView listView;
    private TrackAdapter trackAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorites);

        Realm.init(this);
        realm = Realm.getDefaultInstance();

        listView = findViewById(R.id.listview_favorites);

        trackAdapter = new TrackAdapter(this, R.layout.row_layout_results);
        listView.setAdapter(trackAdapter);

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        RealmResults<Favorites> favorites = realm.where(Favorites.class).findAll();
        for(Favorites favorites1 : favorites){
            if(user.getUid().equals(favorites1.getUser_id())) {
                addToFavoritesActivity(favorites1.getTrack_id());
            }
        }
    }

    public void addToFavoritesActivity(int track_id){
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        String URL = "http://api.musixmatch.com/ws/1.1/track.get" +
                "?track_id=" + track_id +
                "&apikey=341792fef546f6ce706b4cc59bd20ea9";

        JsonObjectRequest objectRequest = new JsonObjectRequest(
            Request.Method.GET,
            URL,
            null,
            new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                try {
                    JSONObject trackJson = response.getJSONObject("message").getJSONObject("body").getJSONObject("track");

                    int track_id = Integer.parseInt(trackJson.getString("track_id"));
                    String track_name = trackJson.getString("track_name");
                    String album_name = trackJson.getString("album_name");
                    String artist_name = trackJson.getString("artist_name");
                    String album_coverart = trackJson.getString("album_coverart_100x100");
                    String release_date = trackJson.getString("first_release_date");

                    Track track = new Track(track_name, album_name, artist_name, album_coverart, release_date, track_id);
                    trackAdapter.add(track);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                }
            },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("resp", error.toString());
                }
            }
        );

        requestQueue.add(objectRequest);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Track track = (Track) trackAdapter.getItem(position);

            Intent intent = new Intent(FavoritesActivity.this, LyricsActivity.class);
            intent.putExtra("track_id", Integer.toString(track.getTrack_id()));
            intent.putExtra("track_name", track.getTrack_name());
            intent.putExtra("artist_name", track.getArtist_name());
            intent.putExtra("album_coverart", track.getAlbum_coverart());
            intent.putExtra("release_date", track.getRelease_date());
            startActivity(intent);
            }
        });
    }
}
