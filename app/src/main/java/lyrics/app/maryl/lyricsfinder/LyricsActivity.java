package lyrics.app.maryl.lyricsfinder;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.json.JSONException;
import org.json.JSONObject;

import io.realm.Realm;
import io.realm.RealmResults;
import lyrics.app.maryl.lyricsfinder.model.Favorites;

/**
 * Created by MARIALENA on 06/06/2018.
 */

public class LyricsActivity extends AppCompatActivity {

    private String track_name, artist_name, album_coverart, release_date;
    private int track_id;
    private TextView txName, tx_artist_name;
    private ToggleButton toggleButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lyrics_display);

        toggleButton = findViewById(R.id.tb_add_to_favorites);
        Realm.init(this);

        Intent myIntent = getIntent();
        track_id = Integer.parseInt(myIntent.getStringExtra("track_id"));
        track_name = myIntent.getStringExtra("track_name");
        artist_name = myIntent.getStringExtra("artist_name");
        album_coverart = myIntent.getStringExtra("album_coverart");
        release_date = myIntent.getStringExtra("release_date");

        txName = findViewById(R.id.tx_track_lyric_name);
        txName.setText(track_name);

        tx_artist_name = findViewById(R.id.tx_lyrics_artist);
        tx_artist_name.setText(artist_name);

        RequestQueue requestQueue = Volley.newRequestQueue(this);

        StringBuilder urlBuilder = new StringBuilder("http://api.musixmatch.com/ws/1.1/");
        urlBuilder.append("track.lyrics.get");
        urlBuilder.append("?track_id="+track_id);
        urlBuilder.append("&apikey=341792fef546f6ce706b4cc59bd20ea9");

        String URL = urlBuilder.toString();

        JsonObjectRequest objectRequest = new JsonObjectRequest(
            Request.Method.GET,
            URL,
            null,
            new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        JSONObject trackList = response.getJSONObject("message").getJSONObject("body").getJSONObject("lyrics");

                        String track_lyrics = trackList.getString("lyrics_body");
                        TextView txLyrics = findViewById(R.id.tx_lyrics);
                        txLyrics.setText(track_lyrics);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("resp", error.toString());
                }
            }
        );

        requestQueue.add(objectRequest);

        Realm backgroundRealm = Realm.getDefaultInstance();
        backgroundRealm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                RealmResults<Favorites> favorites = realm.where(Favorites.class).findAll();
                for (Favorites favorites1 : favorites) {
                    if ((favorites1.getPrimary()).equals(track_id + "_" + user.getUid())) {
                        setIsFavorite(true);
                    }
                }
            }
        });
    }

    @Override
    public void onRestart()
    {
        super.onRestart();
    }

    public void onToggleButtonClicked(View v){

        boolean on = ((ToggleButton) v).isChecked();
        if(on) {
            addRemoveFavorites(track_id, true);
        }
        else {
            addRemoveFavorites(track_id, false);
        }
    }

    public void setIsFavorite(final Boolean isFav) {
        Log.e("set is fav: ", Boolean.toString(isFav));

        LyricsActivity.this.runOnUiThread(new Runnable() {
            public void run() {
                toggleButton.setChecked(isFav);
            }
        });
    }

    public void addRemoveFavorites(final int track_id, final boolean addToFavorites){

        Realm backgroundRealm = Realm.getDefaultInstance();
        backgroundRealm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

                if (addToFavorites) {
                    Favorites favorites = realm.createObject(Favorites.class, track_id + "_" + user.getUid());
                    favorites.setUser_id(user.getUid());
                    favorites.setTrack_id(track_id);
                } else {
                    Favorites fav = realm.where(Favorites.class).equalTo("primary", track_id + "_" + user.getUid()).findFirst();
                    fav.deleteFromRealm();
                }
            }
        });
    }
}