package lyrics.app.maryl.lyricsfinder.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by MARIALENA on 10/06/2018.
 */

public class Favorites extends RealmObject {

    @PrimaryKey
    String primary;
    int track_id;
    String user_id;

    public String getPrimary() {
        return primary;
    }

    public void setPrimary(String primary) {
        this.primary = primary;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public int getTrack_id() {
        return track_id;
    }

    public void setTrack_id(int track_id) {
        this.track_id = track_id;
    }

    @Override
    public String toString() {
        return "Favorite: id("+primary+") track_id("+track_id+") user_id("+user_id+")";
    }
}
