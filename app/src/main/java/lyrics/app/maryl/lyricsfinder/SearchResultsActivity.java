package lyrics.app.maryl.lyricsfinder;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import lyrics.app.maryl.lyricsfinder.adapters.TrackAdapter;
import lyrics.app.maryl.lyricsfinder.model.Track;

/**
 * Created by MARIALENA on 05/06/2018.
 */

public class SearchResultsActivity extends AppCompatActivity {

    private ListView listView;
    private TrackAdapter trackAdapter;
    private String track_name, album_name, artist_name, album_coverart, release_date;
    private int track_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_results);

        listView = findViewById(R.id.listview_results);

        trackAdapter = new TrackAdapter(this, R.layout.row_layout_results);
        listView.setAdapter(trackAdapter);

        Intent myIntent = getIntent();
        String searchParameter = myIntent.getStringExtra("searchParameter");

        RequestQueue requestQueue = Volley.newRequestQueue(this);

        String URL = "http://api.musixmatch.com/ws/1.1/track.search" +
                    "?q_artist=" + searchParameter +
                    "&apikey=341792fef546f6ce706b4cc59bd20ea9";

        JsonObjectRequest objectRequest = new JsonObjectRequest(
            Request.Method.GET,
            URL,
            null,
            new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                try {
                    JSONObject trackList = response.getJSONObject("message").getJSONObject("body");

                    JSONArray jsonArray = trackList.getJSONArray("track_list");

                    for(int i=0; i<jsonArray.length(); i++)
                    {
                        JSONObject jb1 = jsonArray.getJSONObject(i).getJSONObject("track");
                        track_id = Integer.parseInt(jb1.getString("track_id"));
                        track_name = jb1.getString("track_name");
                        album_name = jb1.getString("album_name");
                        artist_name = jb1.getString("artist_name");
                        album_coverart = jb1.getString("album_coverart_100x100");
                        release_date = jb1.getString("first_release_date");

                        Track track = new Track(track_name, album_name, artist_name, album_coverart, release_date, track_id);

                        trackAdapter.add(track);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                }
            },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("resp", error.toString());
                    }
                }
        );

        requestQueue.add(objectRequest);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Track track = (Track) trackAdapter.getItem(position);

            Intent intent = new Intent(SearchResultsActivity.this, LyricsActivity.class);
            intent.putExtra("track_id", Integer.toString(track.getTrack_id()));
            intent.putExtra("track_name", track.getTrack_name());
            intent.putExtra("artist_name", track.getArtist_name());
            intent.putExtra("album_coverart", track.getAlbum_coverart());
            intent.putExtra("release_date", track.getRelease_date());
            startActivity(intent);
            }
        });
    }
}
